from django.shortcuts import render
from .models import Technician, AutomobileVO, Appointment
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder

# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "vip",
        "customer",
        "technician",
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):

    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)
        employee_id = content.get('employee_id')

        if employee_id is None:
            return JsonResponse(
                {"message": "Employee ID is required"},
                status=400,
            )
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )
    
@require_http_methods(["DELETE"])
def api_delete_technician(request, technician_id):

    if request.method == "DELETE":
        try:
            technician = Technician.objects.get(pk=technician_id)
            technician.delete()
            return JsonResponse(
                {"deleted": True},
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400,
            )

@require_http_methods(["GET", "POST"])
def api_list_apointments(request, vin=False):

    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician_id = content.get("technician")
            technician = Technician.objects.get(pk=technician_id)
            content["technician"] = technician
            content["status"] = "created"
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400,
            )
        try:
            vin = content["vin"]
            vip = AutomobileVO.objects.get(vin=vin)
            if vip:
                content["vip"] = True
                appointment = Appointment.objects.create(**content)
        except AutomobileVO.DoesNotExist:
            appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )
    
@require_http_methods(["DELETE"])
def api_delete_appointment(request, appointment_id):

    if request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(pk=appointment_id)
            appointment.delete()
            return JsonResponse(
                {"deleted": True},
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid appointment id"},
                status=400,
            )
    

@require_http_methods(["PUT"])
def api_cancel_appointment(request, appointment_id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(pk=appointment_id)
            appointment.status = "canceled"
            appointment.save()
            return JsonResponse(
                {"message": "Appointment canceled successfully"},
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid appointment id"},
                status=400,
            )
        
@require_http_methods(["PUT"])
def api_finish_appointment(request, appointment_id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(pk=appointment_id)
            appointment.status = "finished"
            appointment.save()
            return JsonResponse(
                {"message": "Appointment finished successfully"},
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid appointment id"},
                status=400,
            )