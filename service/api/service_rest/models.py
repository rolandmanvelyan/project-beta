from django.db import models

# Create your models here.

class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100, unique=True)


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()
    status = models.CharField(max_length=20)
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=100)
    vip = models.BooleanField(default=False, blank=True, null=True)
    technician = models.ForeignKey(
        Technician,
        related_name="service",
        on_delete=models.CASCADE,
        null=True
    )