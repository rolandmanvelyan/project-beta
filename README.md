# AutoHub

AutoHub is a software designed to oversee various functions within a car dealership, including inventory, sales, and service of automobiles.

## About the Team

* Roland Manvelyan - Automobile Service
* Person 2 - Which microservice?

## Getting Started

You will need: Docker, Git, and Node.js 18.2 or above.

After you check and see that you have all the requirements, you will need to follow these step:

 * Fork this repository. Then clone it onto your local computer using the git command:
 ```
 git clone <<repository.url>>
 ```

 * Make sure you are in the root directory of the project, and run the commands:
 ```
 docker volume create beta-data
 docker-compose build
 docker-compose up
 ```
 * Once all of your Docker containers are up and running, you can view AutoHub in the browser:
    http://localhost:3000/

## Design

AutoHub comprises three interconnected backend microservices—Inventory, Sales, and Service—that collaborate to oversee our company's Inventory, Sales, and Service divisions.

## Inventory Microservice

The Inventory microservice consists of three models: Manufacturer, VehicleModel, and Automobile

 * The Manufacturer model tracks a lists of the Vehicle Manufacturers

 * The VehicleModel tracks a list of Vehicle Models by their manufacturer name, model name, and a picture_url that displays a nice photo of the model.

 * The Automobile model tracks the inventory by storing each automobile by its manufacturer, model, VIN, year, and color.

## Service Microservice

The Service microservice consists of three models: Technician,  AutomobileVO, and Appointment

 * The Technician model includes a registered technician's "first_name", "last_name", and "employee_id" fields, helping us manage and monitor our skilled service technicians.

 * The AutomobileVO model is designed to track the "vin" and "sold" fields of the Automobile objects in our inventory. This ensures synchronization between Automobile Value Objects in the Service microservice and our inventory. Achieving this involves a polling function that retrieves "vin" and "sold" data from the Automobile Model in the Inventory microservice every minute. This process keeps the AutomobileVO objects in sync with the Inventory Microservice consistently.

 * The Appointment model includes all the necessary information needed to keep track of service appointments made by the customers. It consists of the fields: "date_time", "reason", "status", "vin", "customer", "vip", and "technician". The "vip" field keeps track of whether the VIN provided for an appointment already exists in the inventory, having that appointment be prioritized for special treatment. Overall, the appointment model facilitates efficient appointment handling within our service department.

## Sales Microservice

Explain your models and integration with the inventory
microservice, here.

## Back-End: Accessing Endpoints to send and see information through Insomnia or your web browser.

### Manufacturers:

You can access the manufacturer endpoints from the following URLs:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/

#### Generating Manufacturer Data using Insomnia:

**POST**: In order to create a manufacturer in the database, send this JSON body that only requires the name of the manufacturer.

```
{
  "name": "Audi"
}
```

**PUT**: You will get the manufacturer's name, href, and id as a return value of creating, viewing, and updating a single manufacturer.

```
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Audi"
}
```

**GET**: The return value of getting a list of manufacturers:

```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Audi"
    }
  ]
}
```

### Vehicle Models:

You can access the vehicle model endpoints from the following URLs:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/

#### Generating Vehicle Model Data using Insomnia:

**POST**: In order to create a vehicle model in the database, send this JSON body that requires the model name, a picture URL, and the id of the manufacturer.

```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
  "manufacturer_id": 1
}
```

**PUT**: You will need the name and/or the picture URL of a vehicle model to update it.

```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
}
```

The return value of creating or updating a vehicle model:

```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```

**GET**: Getting a list of vehicle models returns a list of the detail information with the key "models".

```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "image.yourpictureurl.com",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```

### Automobiles:

You can access the vehicle model endpoints from the following URLs:

Note: The identifier, **"vin"** , is NOT an integer id. It represents the VIN for the specific automobile you want to access. The VIN is a string value.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/

#### Generating Automobile Data using Insomnia:

**POST**: In order to create a automobile in the database, send this JSON body that requires the color, year, VIN, and the id of the vehicle model.

```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```

The return value of creating an automobile:

```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "777",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "R8",
		"picture_url": "image.yourpictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Audi"
		}
	}
}
```


**PUT**: You can update the color, year, and sold status of an automobile.

```
{
  "color": "red",
  "year": 2012,
  "sold": true
}

```

**GET**: Getting a list of automobiles returns the value:

```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "image.yourpictureurl.com",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      }
    }
  ]
}
```

## Diagram

![CarCar Diagram](CarCar_Diagram.png)