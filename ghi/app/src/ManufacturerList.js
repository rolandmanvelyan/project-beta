import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);

        const fetchManufacturers = async () => {
            try {
                const response = await fetch('http://localhost:8100/api/manufacturers/');
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data = await response.json();
                setManufacturers(data.manufacturers);
            } catch (error) {
                console.error('Error fetching manufacturers:', error);
            }
        };

    useEffect(() => {
        fetchManufacturers();
    }, []);

    return (
        <div>
            <h1>Manufacturers</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => (
                        <tr key={manufacturer.href}>
                            <td>{manufacturer.name}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ManufacturerList;