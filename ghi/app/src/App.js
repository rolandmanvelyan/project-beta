import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelList from './VehicleModelList';
import VehicleModelForm from './VehicleModelForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<ManufacturerList maunfacturers={props.maufacturers}/>} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="vehicle-models">
            <Route index element={<VehicleModelList models={props.models}/>} />
            <Route path="new" element={<VehicleModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList automobiles={props.automobiles}/>} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="technicians">
            <Route index element={<TechnicianList technicians={props.technicians}/>} />
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentList appointments={props.appointments}/>} />
            <Route path="new" element={<AppointmentForm />} />
          </Route>
          <Route path="service-history">
            <Route index element={<ServiceHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
