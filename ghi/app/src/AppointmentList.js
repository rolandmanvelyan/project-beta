import React, { useEffect, useState } from "react";

function AppointmentList() {
    const [appointments, setAppointments] = useState([]);

    const fetchAppointments = async () => {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/`);
            if (!response.ok) {
                throw new Error('Response was not ok');
            }
            const data = await response.json();
            setAppointments(data.appointments || []);
        } catch (error) {
            console.error('Error fetching appointments:', error);
        }
    };

    useEffect(() => {
        fetchAppointments();
    }, []);

    const finishAppointment = async (event, id) => {
        event.preventDefault();
        const fetchConfig = {
            headers: {
                "Content-Type": "application/json",
            },
            method: "PUT",
            body: JSON.stringify({status: "finished"}),
        };

        try {
            const response = await fetch(
            `http://localhost:8080/api/appointments/${id}/finish/`,
            fetchConfig
            );
            if (response.ok){
                const data = await response.json();
                fetchAppointments();
            }
        } catch (error) {
            console.error(error);
        }
    };


    const cancelAppointment = async (event, id) => {
        event.preventDefault();

        const fetchConfig = {
            hearders: {
                "Content-Type": "application/json"
            },
            method: "PUT",
            body: JSON.stringify({ status: "canceled"}),
        };

        try {
            const response = await fetch(
            `http://localhost:8080/api/appointments/${id}/cancel/`, 
            fetchConfig
            );
            if (response.ok) {
                const data = await response.json();
                fetchAppointments();
            }
        } catch (error) {
            console.error(error);
        }
    };

    function dateChange(date) {
        const dateTime = new Date(date);
        const dateFormatted = dateTime.toLocaleDateString();
        return dateFormatted;
    }

    function timeChange(date) {
        const dateTime = new Date(date);
        const timeFormatted = dateTime.toLocaleTimeString([], {
            hour: "2-digit",
            minute: "2-digit",
        });
        return timeFormatted;
    } 

    return (
        <>
          <div className="container">
            <h1>Service Appointments</h1>
          </div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {appointments.map((appointment) => {
                if (
                  appointment.status === "canceled" ||
                  appointment.status === "finished"
                ) {
                  return null;
                }
                return (
                  <tr key={appointment.id}>
                    <td>{appointment.vin}</td>
                    <td>{appointment.vip ? "Yes" : "No"}</td>
                    <td>{appointment.customer}</td>
                    <td>{dateChange(appointment.date_time)}</td>
                    <td>{timeChange(appointment.date_time)}</td>
                    <td>
                      {appointment.technician.first_name} {appointment.technician.last_name}
                    </td>
                    <td>{appointment.reason}</td>
                    <td>
                      <button
                        onClick={(event) => cancelAppointment(event, appointment.id)}
                        type="submit"
                        className="btn btn-danger"
                      >
                        Cancel
                      </button>
                    </td>
                    <td>
                      <button
                        onClick={(event) => finishAppointment(event, appointment.id)}
                        type="submit"
                        className="btn btn-success"
                      >
                        Finish
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </>
      );
    }


export default AppointmentList;