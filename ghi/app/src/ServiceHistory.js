import React, { useEffect, useState } from 'react';

function ServiceHistory() {
    const [ appointments, setAppointments ] = useState([]);
    const [ searchVIN, setSearchVIN ] = useState('');

    const fetchAppointments = async () => {
        try {
            const response = await fetch("http://localhost:8080/api/appointments/");
            if (response.ok) {
                const data = await response.json();
                setAppointments(data.appointments);
            } else {
                console.error(response);
            }
        } catch (error) {
            console.error("Error fetching appointments:", error)
        }
    }

    useEffect(() => {
        fetchAppointments();
    }, []);

    function dateChange(date) {
        const dateTime = new Date(date);
        const dateFormatted = dateTime.toLocaleDateString();
        return dateFormatted;
    }

    function timeChange(date) {
        const dateTime = new Date(date);
        const timeFormatted = dateTime.toLocaleTimeString([], {
            hour: "2-digit",
            minute: "2-digit",
        });
        return timeFormatted;
    } 

    const searchAppointments = appointments.filter(appointment =>
      appointment.vin.includes(searchVIN.toUpperCase())
      );

    return (
        <>
          <div className="container m-3">
            <h1>Service History</h1>
          </div>
          <div className="input-group mb-3">
            <input onChange={(e) => setSearchVIN(e.target.value)} value={searchVIN} required type="text" className="form-control m-3" placeholder="Search by VIN..." aria-label="Search" aria-describedby="basic-addon2" />
            <div className="input-group-append m-3">
              <button className="btn btn-outline-primary" required type="button">Search</button>
            </div>
          </div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {searchAppointments.map((appointment) => {
                        return (
                            <tr key={appointment.id}>
                    <td>{appointment.vin}</td>
                    <td>{appointment.vip ? "Yes" : "No"}</td>
                    <td>{appointment.customer}</td>
                    <td>{dateChange(appointment.date_time)}</td>
                    <td>{timeChange(appointment.date_time)}</td>
                    <td>
                      {appointment.technician.first_name} {appointment.technician.last_name}
                    </td>
                    <td>{appointment.reason}</td>
                    <td>{appointment.status}</td>
                  </tr>
                );
            })}
            </tbody>
          </table>
            </>
    );

}

export default ServiceHistory;