import React, { useEffect, useState } from "react";

function VehicleModelList() {
    const [models, setModels] = useState([]);

    const fetchModels = async () => {
        try {
            const response = await fetch('http://localhost:8100/api/models/');
            if (!response.ok) {
                throw new Error('Response was not ok');
            }
            const data = await response.json();
            setModels(data.models);
        } catch (error) {
            console.error('Error fetching vehicle models:', error);
        }
    };
    
    useEffect(() => {
        fetchModels();
    }, []);

    return (
        <div>
            <h1>Vehicle Models</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => (
                        <tr key={model.href}>
                            <td>{model.name}</td>
                            <td>{model.manufacturer.name}</td>
                            <td>
                                <img src={model.picture_url} className="img-thumbnail" alt={model.name}></img>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default VehicleModelList;