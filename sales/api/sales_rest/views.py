from django.shortcuts import render
import json
from .models import Customer, Salesperson, Sale
from common.json import ModelEncoder

# Create your views here.

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "amount",
    ]
    # The Automobile VIN
    # The Salesperson
    # The Customer#
