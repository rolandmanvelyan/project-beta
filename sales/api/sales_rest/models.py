from django.db import models

# Create your models here.

class Salesperson(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    employee_id = models.CharField(max_length=150, unique=True)

class Customer(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    address = models.CharField(max_length=150)
    phone_number = models.CharField(max_length=150)

class Sale(models.Model):
    # The Automobile
    # The salesperson
    # The Customer
    amount = models.IntegerField()


